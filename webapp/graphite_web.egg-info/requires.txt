Django<3.1,>=1.8
django-tagging==0.4.3
pytz
pyparsing
cairocffi
urllib3
six

[:python_version < "3.5"]
scandir
